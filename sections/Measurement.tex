\section{Measurement of the charge misidentification efficiencies}
\label{sec:measurement}

%-------------------------------------------------------------------------------


Charge-flip probability is measured by performing a selection of \Zee events by requiring
the pairs of electrons to be inside the \Zboson mass window. Most of the background
(non \Zee events) is composed of top physics, diboson events, and fake electrons from \Wjets events
Two types of \Zee events are used for the charge-flip measurement--- opposite-charge (OC) and
same-charge (SC) events.

\subsection{Sideband subtraction}
\label{sec:sideband}

The \Zee region is divided into three regions: the main region and two sidebands orthogonal to the
main region. The purpose of the sidebands is to estimate the non-\Zboson background in data and
subtract it from the main data region. This data-driven method of background subtraction is called the
\textit{sideband} method. The method is performed only in the data charge misidentification measurement
since truth information is used in MC to select only prompt electrons, as discussed further in \Sect{sec:FSR}.
An assumption that non-\Zboson backgrounds have flat distributions in this invariant mass range is made and
the width of the bands has to be the same as the central region or scaled to the same size.
For the measurement presented in this note, a \SI{20}{\GeV} wide main region is selected along
with two \SI{20}{\GeV} sidebands, as summarized in \Tab{\ref{tab:chflip-regions}}. 

\begin{table}[htbp]
\caption[Definitions of main regions and sideband regions for opposite-charge and same-charge type \Zboson peak events.]
{
Definitions of main regions and sideband regions for opposite-charge and same-charge type \Zboson
peak events. The regions are centered around \SI{90}{\GeV}, denoted as $m_{\Zboson}$.
}
\label{tab:chflip-regions}
\begin{center}
\begin{tabular}{ l r r }
\toprule
Event type      & invariant mass region    &  event weight    \\
\midrule
Main region     & $|m(ee) - m_{\Zboson}| < \SI{10}{\GeV}$  &  1.0 \\
Sideband region & $\SI{10}{\GeV} < |m(ee) - m_{\Zboson}| < \SI{30}{\GeV}$ & 0.5 \\ 
\bottomrule
\end{tabular}
\end{center}
\end{table}

Some of the signal (\Zee) is subtracted along the fakes as well, however, that does not affect the
charge-flip rate as all electrons are the same (for a small enough $[\pt, \eta]$ region
of the first electron, the invariant mass formed with the second electron will not affect the
charge-flip rate). Effectively, some statistics are lost due to the sideband subtraction method
because some signal electrons are removed, but the effect is very small overall (i.e. $<\SI{1}{\percent}$).


\subsection{Final state radiation}
\label{sec:FSR}

The sideband subtraction method discussed in \Sect{\ref{sec:sideband}} is used to subtract the flat
non-\Zboson background from the main data region. However, special treatment is needed for electrons
originating from final state radiation (FSR electrons). FSR electrons are normally treated as prompt,
however, there are currently no prescriptions on how to determine the true charge of these electrons.
Charge misidentification scale factors derived in this measurement therefore can not be used for FSR
electrons and charge misidentification probabilities are measured accordingly in MC and data.

In the MC charge misidentification probability measurement, only non-FSR prompt electrons are used.
They are classified using \texttt{MCTruthClassifier}~\cite{webMCTruthClassifier} by requiring
$\texttt{firstEgMotherType}=2$ and $\texttt{firstEgMotherOrigin}=13$. On the other hand, FSR
electrons in MC are classified with $\texttt{firstEgMotherType}=15$ and $\texttt{firstEgMotherOrigin}=40$.
They are subtracted from main and sideband data regions to achieve a compatible measurement in data
and MC. Since the event selection in the charge misidentification measurement requires two electrons,
an event is classified as originating from FSR if at least one of the electrons in the event is
recognized as a FSR electron. The truth matching is summarized in \Tab{\ref{tab:truth-matching}}.

\begin{table}[htbp]
\caption
{
Truth matching used in MC to determine signal and FSR electrons.
}
\label{tab:truth-matching}
\begin{center}
\begin{tabular}{ l | r r }
\toprule
                & \multicolumn{2}{c}{\texttt{firstEgMotherType}/\texttt{firstEgMotherOrigin}}    \\
Event type      & first electron    &  second electron   \\
\midrule
Signal                & 2/13              & 2/13               \\
\midrule
\multirow{ 3}{*}{FSR} & 15/40             & 2/13               \\
                      & 2/13              & 15/40              \\
                      & 15/40             & 15/40              \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\subsection{The likelihood function}
\label{sec:likelihood}

After the background subtraction, it is assumed that one of the electrons in the pair has incorrectly measured
charge, because they originate
from the \Zee events. The total number of electron pairs is denoted by $N^{ij}$, where $i$ stands for the
leading electrons bin and $j$ for the sub-leading electrons bin. Charge-flip probability is measured as a
function of \pt and $\eta$, therefore $i$ and $j$ are both two dimensional bins ($({\pt}_i, \eta_i)$).
The total number of electron pairs can be divided into same-charge and opposite-charge pairs:
$N^{ij}=N^{ij}_\mathrm{SS} + N^{ij}_\mathrm{OS}$ and the probability of observing
$N^{ij}_\mathrm{SS}$ same-charge events is a Poissonian probability:

\begin{equation}
  f(N^{ij}_\mathrm{SC};\lambda')=\frac{\lambda'^{N^{ij}_\mathrm{SC}} e^{-\lambda'}}{N^{ij}_\mathrm{SC}!},
  \label{eq:chflip-poisson}
\end{equation}

where $\lambda' = (\epsilon_i(1-\epsilon_j) + \epsilon_j(1-\epsilon_i))N^{ij}$ is the expected
number of same-charge events in bin $(i,j)$ given the charge misidentification probabilities
$\epsilon_i$ and $\epsilon_j$ and $N^{ij}_\mathrm{SC}$ is the measured number of same-charge events.

Furthermore, to account for the sideband subtraction scheme, the expected number of same-charge events in
data is constructed as a sum of two terms: signal term multiplied with the charge misidentification probability
plus the background term, corresponding to background from sideband subtraction and FSR---

\begin{equation}
  \lambda' \to \lambda^{ij} = (\epsilon_i(1-\epsilon_j) + \epsilon_j(1-\epsilon_i))N^{ij} + \mathrm{BKG}^{ij}_\mathrm{SC} + \mathrm{FSR}^{ij}_\mathrm{SC},
  \label{eq:background}
\end{equation}

where $\mathrm{BKG}^{ij}_\mathrm{SC}$ and $\mathrm{FSR}^{ij}_\mathrm{SC}$ are the background terms and
$N^{ij}$ includes only signal electrons (achieved with sideband and FSR subtraction). No subtraction
is then performed for $N^{ij}_\mathrm{SC}$, to reflect that $\lambda^{ij}$ already includes the background.

The charge-flip rates are extracted with a \textit{likelihood fit} by summing all of the Poissonian
probabilities in \Eqn{\ref{eq:chflip-poisson}} and maximizing the likelihood by varying the
charge-flip probabilities. A standard procedure to do this is to construct a negative logarithm of
the summed likelihood:

\begin{equation}
  -\log L \left( \vec{\epsilon} | \vec{N_\mathrm{SC}}, \vec{N} \right) \approx
  \sum\limits_{i,j} \log \left(\lambda^{ij} \right) N^{ij}_\mathrm{SC}
  - \lambda^{ij}.
\label{eq:chflip-loglikelihood}
\end{equation}

The constant terms in the denominator of \Eqn{\ref{eq:chflip-poisson}} were not included into the
likelihood function because they do not have any effect on the minimization. The likelihood function
is minimized using the \textsc{ROOT} minimization interface with the \textsc{Minuit2} package and
\textsc{Migrad} algorithm.

\subsection{Parametrization of the charge misidentification probability}
\label{sec:parametrization}

The standard double-differential (2D) measurement is performed in bins of \pt and $|\eta|$ corresponding
to $[20.0, 30.0, 35.0, 40.0, 45.0, 60.0, 95.0, 115, 145, 185, 240, )$ for \pt and
$[0.00, 0.75, 1.25, 1.52, 1.70, 2.30, 2.50]$ for $|\eta|$ amounting in total to $12\times6 = 72$ free
parameters in the likelihood fit. Because the measurement is performed at the $\Zee$ peak, available
statistics drop rapidly for electrons above \SI{45}{\GeV} and the statistical uncertainty becomes
prohibitively large above \SI{95}{\GeV}.

To tackle the issue with low statistics at high-\pt, a 1D$\times$1D parametrization is introduced,
where the charge misidentification probability is parametrized as a product of two histograms.
An assumption is made that the shape of the $\eta$ dependence is very similar for all \pt bins and
thus the parametrization can be expressed as a product of two one-dimensional functions:

\begin{equation}
  \epsilon(\pt, |\eta|) = \sigma(\pt) \times f(|\eta|).
\label{eq:1Dparametrization}
\end{equation}

Further, one of the functions is required to be normalized to unit area ($\int f(\eta) \dif \eta = 1$)
which decreases the number of free parameters to $N_{eta} + N_{\pt} - 1$ compared to the number of
parameters $N_\eta \times N_{\pt}$ in the two-dimensional parametrization. Normalizing one of the
functions ensures existence of a non-trivial solution of the likelihood function. Since less
degrees of freedom are needed to describe the charge misidentification probability with this parametrization,
$|\eta|$ granularity can be even further improved to
$[0.00, 0.50, 0.75, 1.05, 1.25, 1.37, 1.52, 1.62, 1.70, 1.80, 1.90, 2.00, 2.10, 2.30, 2.40, 2.50]$,
amounting to $12 + 15 - 1 = 26$ free parameters.

The requirement of unit area of the $f(\eta)$ histogram
is realized in the likelihood fit by adding a constraint to the likelihood function

\begin{equation}
  \alpha \left( \sum\limits_{n=1}^{N_{\eta}} \left[ f(i) \times \Delta \eta_i \right] - 1 \right)^2,
\label{eq:normalizationconstraint}
\end{equation}

where $f(i)$ is the value of the $i$-th bin of the $f(\eta)$ function, $\Delta \eta_i$ is the
width of the $i$-th bin and the sum which goes over all bins of the $f(\eta)$ function yields
the area under the function. Parameter $\alpha$ is an arbitrary scale which is set in a way
to minimize the correlations between the free parameters in the likelihood fit, as described
further in \Sect{\ref{sec:1Dresults}}.

\subsection{Systematic uncertainty}
\label{sec:sys}

Systematic uncertainties affecting both the 2D and the 1D measurement are: statistical uncertainty
due to finite number of \Zee events, uncertainty due to sideband subtraction, and uncertainty due
to FSR subtraction. In addition to this, the 1D measurement has an additional uncertainty due to
the assumption that \pt and $|\eta|$ effects on charge misidentification probability are decorrelated.
Moreover, for the 2D measurement, the difference between the charge misidentification probability
derived with the likelihood fit and the true misidentification probability in MC is taken as a source
of uncertainty. Because of the nature of the 1D$\times$1D measurement, only the shape of the $|\eta|$
dependence on the charge misidentification probability can be compared and no additional uncertainty
is assigned. Details about the size of systematic uncertainties and the methods used to derive them
are presented in \Refs{\ref{sec:2Dresults}}{\ref{sec:1Dresults}} and all sources are summarized in
\Tab{\ref{tab:systematics}}.

\begin{table}[htbp]
\caption
{
Sources of systematic uncertainties affecting the 2D and the 1D measurements of the charge
misidentification probability.
}
\label{tab:systematics}
\begin{center}
\begin{tabular}{ l r r r r r}
\toprule
Source      & statistical    &  sideband sub.  & FSR sub. & truth comparison & 1D non-closure    \\
\midrule
2D data     & \ding{51}      &  \ding{51}             & \ding{51}       & --               & --                \\
2D MC       & \ding{51}      &  --                    & --              & \ding{51}        & --                \\
1D$\times$1D data & \ding{51}& \ding{51}              & \ding{51}       & --               & \ding{51}         \\
1D$\times$1D data & \ding{51}& --                     & --              & --               & \ding{51}         \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

